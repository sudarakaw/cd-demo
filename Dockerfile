FROM alpine

RUN apk --update add --no-cache bash git libssh2-dev build-base

RUN wget https://curl.se/download/curl-7.76.1.tar.gz \
    && tar xf curl-7.76.1.tar.gz \
    && cd curl-7.76.1 \
    && ./configure --with-libssh2=/usr/local \
    && make \
    && make install \
    && cd .. && rm -fr curl-7.76.1

RUN git clone https://github.com/git-ftp/git-ftp.git /opt/git-ftp-src \
    && cd /opt/git-ftp-src \
    && tag="$(git tag | grep '^[0-9]*\.[0-9]*\.[0-9]*$' | tail -1)" \
    && git checkout "$tag" \
    && make install \
    && rm -fr  /opt/git-ftp-src
