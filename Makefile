# Makefile: build/deploy scripts
#
# Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

DEPLOY_HOST ?=

all: build

build: web/index.html

web/%: src/% web
	cp $< $@

.ONESHELL:
web:
	mkdir -p $@/assets/css
	cat > $@/assets/css/style.css << EOF
	body { color: red; }
	EOF

.PHONY: clean
clean:
	rm -fr web/

.PHONY: update
update: src/index.html
	sed -i "s/\(<!-- new-entry -->\)/\1\n      <li>`date`<\/li>/" $<

.PHONY: docker-image
docker-image: Dockerfile
	-docker rmi -f registry.gitlab.com/sudarakaw/cd-demo
	docker build -t registry.gitlab.com/sudarakaw/cd-demo .

.PHONY: docker-push
docker-push: docker-image
	docker push registry.gitlab.com/sudarakaw/cd-demo

.PHONY: deploy
deploy:
	git-ftp push \
		$(filter-out deploy $@,$(MAKECMDGOALS)) \
		--user ${DEPLOY_USER} \
		--key ${DEPLOY_PRIVATE_KEY} \
		--insecure \
		--syncroot=web/\
		--verbose \
		--auto-init \
		sftp://${DEPLOY_HOST}:${DEPLOY_PORT}/http/${DEPLOY_USER}/web

    #   sftp -P ${DEPLOY_PORT} -i ${DEPLOY_PRIVATE_KEY} -o UserKnownHostsFile=${DEPLOY_KNOWN_HOSTS} ${DEPLOY_USER}@${DEPLOY_HOST} << EOF
